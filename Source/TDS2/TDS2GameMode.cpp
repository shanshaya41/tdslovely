// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "TDS2GameMode.h"
#include "TDS2PlayerController.h"
#include "TDS2Character.h"
#include "UObject/ConstructorHelpers.h"

ATDS2GameMode::ATDS2GameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = ATDS2PlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Blueprints/Character/TopDownCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}